var optipng = require('imagemin-optipng');
var jpegtran = require('imagemin-jpegtran');
var gifsicle = require('imagemin-gifsicle');
var svgo = require('imagemin-svgo');
var serveStatic = require('serve-static');

module.exports = function(grunt) {
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		sass: {
			dist: {
				options: {
					loadPath: require('node-neat').with('node-bourbon')
				},
				files: {
					'app/css/app.css' : 'dev/scss/app.scss'
				}
			}
		},
		postcss:{
			options: {
				map: true,
				processors: [
					require('autoprefixer')({browsers: ['last 4 version']})
				]
			},
			dist:{
				files:{
					'app/css/min/app.css':'app/css/app.css'
				}
			}
		},
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'app/css/app.min.css': ['app/css/app.css']
				}
			}
		},
		uglify: {
			build: {
				files: [{
		            expand: true,
		            src: ['**/*.js'],
		            dest: 'app/js/',
		            cwd: 'dev/js/'
		        }]
			}
		},
		jade: {
			compile: {
				options: {
					client: false,
					pretty: true,
					data: {
						debug: false
					}
				},
				files: [{
					expand: true,
					flatten: false,
					cwd: "dev/",
					src: ["**/*.jade"],
					dest: "app/",
					ext: ".html"
				}]
			}
		},
		imagemin:{
			target: {
				options: {
					optimizationLevel: 5,
					progressive: true,
					use: [optipng(), jpegtran(), gifsicle(), svgo()]
				}, // options
				files: [{
					expand: true,
					cwd: 'dev/img/',
					src: ['**/*.{png,jpg,jpeg,gif,svg}'],
					dest: 'app/img/'
				}] // files
			} // target
		},
		watch: {
			options: {
		      	livereload: true,
		    },
			css: {
				files: 'dev/**/*.scss',
				tasks: ['sass','postcss','cssmin']
			},
			scripts: {
				files: ['dev/js/*.js'],
				tasks: ['uglify']
			},
			img: {
				files: ['dev/img/*.*'],
				tasks: ['imagemin']
			},
			jade: {
				files: ["dev/**/*.jade"],
				tasks: ['jade']
			}
		},
		connect: {
			server: {
				options: {
					port: 8080,
					base: './app/',
					hostname: '0.0.0.0',
					protocol: 'http',
					livereload: true,
					open: true,
				}
			}
		},
		browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        'app/css/*.css',
                        'app/css/**/*.css',
                        'app/*.html'
                    ]
                },
                options: {
                    watchTask: true,
                    server: './app'
                }
            }
        }
	});

	grunt.registerTask('default',['watch']);
	grunt.registerTask('server-static', ['connect','watch']);
	grunt.registerTask('server', ['browserSync','watch']);
};
