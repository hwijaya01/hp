// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var changed = require('gulp-changed');
var imagemin = require('gulp-imagemin');
var imageminGifsicle = require('imagemin-gifsicle');
var imageminJpegtran = require('imagemin-jpegtran');
var imageminOptipng = require('imagemin-optipng');
var imageminSvgo = require('imagemin-svgo');
var autoprefixer = require('autoprefixer');
var postcss = require('gulp-postcss');
var mqpacker = require('css-mqpacker');
var csswring = require('csswring');
var jade = require('gulp-jade');
var server = require('gulp-server-livereload');
var browserSync = require('browser-sync').create();

// Lint Task
gulp.task('lint', function() {
    return gulp.src('dev/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src('dev/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('app/css'));
});

// Css postprocessor
gulp.task('css', function () {
    var processors = [
        autoprefixer({browsers: ['last 4 version']}),
        mqpacker,
        csswring
    ];
    return gulp.src('app/css/*.css')
        .pipe(postcss(processors))
        .pipe(gulp.dest('app/css/min'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src('dev/js/*.js')
        .pipe(concat('app.js'))
        .pipe(gulp.dest('app/js/'))
        .pipe(rename('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/js/'));
});

// minify new images
gulp.task('imagemin', function() {
    var imgSrc = 'dev/img/**/*',
        imgDst = 'app/img';

    gulp.src(imgSrc)
    .pipe(changed(imgDst))
    .pipe(imagemin({
        optimizationLevel: 5, 
        progressive: true, 
        interlaced: true
    }))
    .pipe(gulp.dest(imgDst));
});

// Jade
gulp.task('jade', function() {
    return gulp.src('dev/**/*.jade')
        .pipe(jade())
        .pipe(gulp.dest('app/'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('dev/js/*.js', ['lint', 'scripts']);
    gulp.watch('dev/scss/*.scss', ['sass','css']);
    gulp.watch('dev/img/*', ['imagemin']);
    gulp.watch('dev/**/*.jade', ['jade']);
});

// Server
gulp.task('webserver', function() {
    gulp.src('.')
    .pipe(server({
      livereload: true,
      directoryListing: true,
      open: true,
      port: 8080
    }));
});

// Static Server + watching scss/html/js files
gulp.task('serve', ['sass'], function() {

    browserSync.init({
        server: "./app"
    });

    gulp.watch('dev/js/*.js', ['lint', 'scripts']);
    gulp.watch('dev/scss/*.scss', ['sass','css']);
    gulp.watch('dev/img/*', ['imagemin']);
    gulp.watch('dev/**/*.jade', ['jade']);
    gulp.watch("app/**/*.html").on('change', browserSync.reload);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'watch']);
gulp.task('server', ['serve'], browserSync.reload);